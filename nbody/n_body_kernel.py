import numpy as np

def n_body_solve(body_masses,
                radii,
                init_positions,
                init_velocities,
                time_interval,
                timestep,
                store_values = False) :
    '''
    Solve an N-Body problem for the system defined by the parameters 
    body_masses, init_velocities, and init_positions, over the provided time 
    interval using the provided timestep.

    Parameters:
        body_masses: An array containing the masses of the bodies in the system 
            in kg
        radius: An array containing the radii of the bodies in meters
        init_positions: An array containing the vector positions of the bodies 
            in meters
        init_velocities: An array containing the vector positions of the bodies 
            in meters per second
        time_interval: The length of time in seconds over which to solve for the
            motion of the bodies.
        timestep: The delta-t value used to coumpute the solution, in seconds.
        store_values: True if the function should return all the calculated 
            positions in an array

    Returns:
        An array containing the vector positions of the bodies at the end of the
        simulation, or if store_values is True, an array containing this 
        position data at each timestep.

    Authors:
        Daniel Rebain 
        Patrick McManus 
        Carl Marais
        Daniel Crossley-Wing
    '''
 
    masses, radii, positions, velocities, time_interval, timestep = verification(body_masses,
                                                                                 radii,
                                                                                 init_positions,
                                                                                 init_velocities,
                                                                                 time_interval,
                                                                                 timestep)
    G = 6.67E-11
    rel_change_warn_threshold = 0.1

    time = 0.0
    n = len(masses)

    output_positions = []
    output_positions.append(positions)
    
    inital_angular_momentum = np.array([0.,0.,0.])
    for i in range(n) :
        inital_angular_momentum += np.cross(positions[i],masses[i] * velocities[i])

    while time < time_interval : 

        time += timestep
        
        b = 0
        
        while b <= (n-1):
            
            i = 0
            while i <= (n -1): 
                
                if i == b:
                    i +=1
                    
                if i <= (n-1):
                    acceleration = (masses[i]*G)*(positions[i]-positions[b])/(np.linalg.norm(positions[i]-positions[b])**3)
                    velocities[b] += timestep * acceleration
                    
                    
                
                    if np.linalg.norm(positions[b] - positions[i]) <= radii[i] :
                        b = n
                        i = n
                        time = time_interval
                        print "There was a collision"
                  
                i += 1


            b += 1

        positions += velocities * timestep

        if store_values :
            output_positions.append(positions)


    final_angular_momentum = np.array([0.,0.,0.])
    for i in range(n) :
        final_angular_momentum += np.cross(positions[i],masses[i] * velocities[i])

    rel_angular_momentum = 100. * np.linalg.norm(final_angular_momentum - inital_angular_momentum) / np.linalg.norm(inital_angular_momentum)

    print "L:",rel_angular_momentum,"%"

    if rel_angular_momentum > rel_change_warn_threshold :
        print "Warning: Relative change in angular momentum greater than",rel_change_warn_threshold,"%"

    if store_values :
        return output_positions
    else :
        return positions
                                                      

  
def verification(body_masses,
                radii,
                init_positions,
                init_velocities,
                time_interval,
                timestep) :
    '''                                                                                                           
    Sets all input parameters to appropriate form. 
    Verifies that the spacial dimension is consistent for all bodies and parameters.
    Verifies that the number of bodies is consistent for all parameters.

    Parameters:
        body_masses: A list containing the masses of the bodies in the system 
            in kg
        radii: A list containing the radii of the bodies in meters
        init_positions: A list containing the vector positions of the bodies 
            in meters
        init_velocities: A list containing the vector positions of the bodies 
            in meters per second
        time_interval: The length of time in seconds over which to solve for the
            motion of the bodies.
        timestep: The delta-t value used to coumpute the solution, in seconds.

    Returns:
        masses: An array containing the masses of the bodies in the system 
            in kg
        radii: An array containing the radii of the bodies in meters
        positions: An array containing the vector positions of the bodies 
            in meters
        velocities: An array containing the vector positions of the bodies 
            in meters per second
        time_interval: The length of time in seconds over which to solve for the
            motion of the bodies.
        timestep: The delta-t value used to coumpute the solution, in seconds.

    Author:
        Daniel Crossley-Wing
    '''

    import sys

    positions = np.array(init_positions, dtype = "float")
    velocities = np.array(init_velocities, dtype = "float")
    masses = np.array(body_masses, dtype = "float")
    radii = np.array(radii, dtype = "float")
    time_interval = float(time_interval)
    timestep = float(timestep)

    n = len(init_positions)
    dimensions = len(init_positions[0])

    if len(body_masses) != n :
        sys.exit('The list of masses and the list of initial positions have a different number of bodies')
    if len(init_velocities) != n :
        sys.exit('The list of initial velocities and the list of initial positions have a different number of bodies')
    if len(radii) != n :
        sys.exit('The list of radii and the list of initial positions have a different number of bodies')

    for i in range(n) :
        if len(init_positions[i]) != dimensions or len(init_velocities[i]) != dimensions :
            sys.exit('The dimension is not consistent')
            
    return masses, radii, positions, velocities, time_interval, timestep
