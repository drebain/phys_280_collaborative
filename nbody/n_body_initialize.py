import numpy as np

def initialize(N = 1, units = "SI"):
    '''
    Takes from the user a set of initial position vectors, initial
    velocity vectors, masses and radii, for N number of bodies, as
    well as the timestep and total time interval for which the
    scenario will run.

    Parameters:
        N: An integer that represents the desired number of bodies
           in the scenario
        units: A string signifying the type of units to be used.
               For Position, Velocity, Mass and Radius:
                   "Earth" corresponds to AU, km/s, Earth masses and
                   Earth radii.
                   "Sun" corresponds to Solar radii, km/s, Solar 
                   masses and Solar radii

    Output:
        A 6-tuple of arrays representing the mass, radius, initial
        position vector, initial velocity vector, time interval

    Author: Adam Inch
    '''
    
    AU_m = 149597870.
    kms_ms = 1000.
    ME_kg = 5.972e24
    MS_kg = 1.989e30
    RE_m = 6.371e6
    RS_m = 6.958e8

    P_Array = []
    V_Array = []
    M_Array = []
    R_Array = []

    count = 0
    while count < N:
        Val_List = AddBody(count)
        P_Array.append(Val_List[0])
        V_Array.append(Val_List[1])
        M_Array.append(Val_List[2])
        R_Array.append(Val_List[3])
        count += 1

    P_Array = np.asarray(P_Array)
    V_Array = np.asarray(V_Array)
    M_Array = np.asarray(M_Array)
    R_Array = np.asarray(R_Array)

    if units == "Earth":
        P_Array *= AU_m
        V_Array *= kms_ms
        M_Array *= ME_kg
        R_Array *= RE_m

    if units == "Sun":
        P_Array *= RS_m
        V_Array *= kms_ms
        M_Array *= MS_kg
        R_Array *= RS_m

    Time_Interval = float(raw_input("How long would you like the program to run for? "))
    TimeStep = float(raw_input("What would you like the timestep to be? "))

    return (M_Array, R_Array, P_Array, V_Array, Time_Interval, TimeStep)

def AddBody(count):
    print "For body number ", count + 1, ":"
    P = raw_input("What is the initial position vector of this body in the form Px,Py,Pz? ")
    P = map(float,P.split(','))

    V = raw_input("What is the initial velocity vector of this body in the form Vx,Vy,Vz? ")
    V = map(float,V.split(','))

    R = float(raw_input("What is the radius of this body? "))
    M = float(raw_input("What is the mass of this body? "))

    return (P,V,R,M)
