import visual as vis

def visualize_n_body(positions,radii,colours,timestep,playback_scale = 1.) :
    '''
    Display a VPython animation of the output of n_body_solve.

    Parameters: 
        positions: A three-dimensional array containing the
            positions of the bodies in the system at each 
            timestep
        radii: An array containing the radius of each body in 
            the system
        colours: An array containing the colour of each body
        timestep: The time interval between consecutive sets
            of values.
        playback_scale: A float value equal to the desired
            playback speed as a multiple of real time

    Authors:
        Daniel Rebain
        Steph Johansen 
    '''
    vis.display()
    spheres = []
    for i in range(len(radii)) :
        spheres.append(vis.sphere(pos = positions[0][i],radius = radii[i],color = colours[i]))

    time = 0.
    for position_set in positions :
        vis.rate(playback_scale / timestep)
        for i in range(len(spheres)) :
            spheres[i].pos = position_set[i]


    