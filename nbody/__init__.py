'''
This module contains functions for solving an arbitrary N-Body problem.

Authors:
    Daniel Rebain
    Patrick McManus
    Carl Marais
    Adam Inch
    Steph Johansen 

'''

from n_body_initialize import *
from n_body_kernel import *
from n_body_visualization import *